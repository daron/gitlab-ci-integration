# BUILD APPLICATION
FROM node:18-alpine AS build

WORKDIR /build

COPY . /build

RUN cd /build && \
    npm ci --include=dev --frozen-lockfile && \
    npm run build

# ---------------------------------------------------

FROM alpine:3.18.4

RUN apk add --update nodejs=~18

WORKDIR /app

COPY --from=build /build/dist/* ./

ENTRYPOINT ["node", "index.js"]
