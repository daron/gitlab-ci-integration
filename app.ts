import process from 'node:process';

import yargs from 'yargs';

import { scanCode } from './src';
import { TimeoutReachedException } from './src/exceptions/timeoutReached.exception';

import type { TCommandLineInputProperties, TInputContext, TInputProperties } from './types';

const argv = yargs(process.argv.slice(2))
	.options({
		secret_key: { type: 'string', demandOption: true },
		fail_on_timeout: { type: 'boolean', default: true },
		fail_on_dependency_scan: { type: 'boolean', default: true },
		fail_on_sast_scan: { type: 'boolean', default: false },
		fail_on_iac_scan: { type: 'boolean', default: false },
		minimum_severity: { type: 'string', default: 'CRITICAL' },
		timeout_seconds: { type: 'number', default: 120 },
	})
	.help()
	.alias('help', 'h').argv as TCommandLineInputProperties;

void (async () => {
	const properties: TInputProperties = {
		secretKey: argv.secret_key,
		failOnTimeout: argv.fail_on_timeout ?? true,
		failOnDependencyScan: argv.fail_on_dependency_scan,
		failOnSastScan: argv.fail_on_sast_scan,
		failOnIacScan: argv.fail_on_iac_scan,
		minimumSeverity: argv.minimum_severity,
		timeoutSeconds: argv.timeout_seconds,
	};

	if (!['LOW', 'MEDIUM', 'HIGH', 'CRITICAL'].includes(properties.minimumSeverity.toUpperCase())) {
		throw new Error(
			`invalid minimum severity provided. The severity must be one of the following values: ['LOW', 'MEDIUM', 'HIGH', 'CRITICAL'], but received: "${properties.minimumSeverity}"`
		);
	}

	const mergeRequestProjectUrl = process.env.CI_MERGE_REQUEST_PROJECT_URL ?? '';
	const mergeRequestInternalId = process.env.CI_MERGE_REQUEST_IID ?? '';

	const context: TInputContext = {
		projectId: parseInt(process.env.CI_PROJECT_ID ?? '0'),
		startCommitId: process.env.CI_MERGE_REQUEST_DIFF_BASE_SHA || process.env.CI_COMMIT_BEFORE_SHA || '',
		endCommitId: process.env.CI_MERGE_REQUEST_SOURCE_BRANCH_SHA || process.env.CI_COMMIT_SHA || '',
		ref: process.env.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME || process.env.CI_COMMIT_BRANCH || '',
		author: process.env.CI_COMMIT_AUTHOR ?? '',
		pullRequestMeta: {
			title: process.env.CI_MERGE_REQUEST_TITLE ?? '',
			url: `${mergeRequestProjectUrl}/-/merge_requests/${mergeRequestInternalId}`,
		},
	};

	try {
		await scanCode(properties, context);
	} catch (err) {
		if (err instanceof Error) {
			console.log(err.message);
		}

		if (err instanceof TimeoutReachedException && properties.failOnTimeout === false) {
			process.exit(0);
		}

		process.exit(1);
	}

	console.log('scan completed without errors');
	process.exit(0);
})();
