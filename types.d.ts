export interface TCommandLineInputProperties {
	secret_key: string;
	fail_on_timeout: boolean;
	fail_on_dependency_scan: boolean;
	fail_on_sast_scan: boolean;
	fail_on_iac_scan: boolean;
	minimum_severity: string;
	timeout_seconds: number;
}

export interface TInputProperties {
	secretKey: string;
	failOnTimeout: boolean;
	failOnDependencyScan: boolean;
	failOnSastScan: boolean;
	failOnIacScan: boolean;
	minimumSeverity: string;
	timeoutSeconds: number;
}

export interface TInputContext {
	projectId: number;
	startCommitId: string;
	endCommitId: string;
	ref: string;
	author: string;
	pullRequestMeta: {
		title: string;
		url: string;
	};
}
