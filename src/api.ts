import axios, { AxiosError } from 'axios';

const AIKIDO_API_URL = 'https://app.aikido.dev';

type TGetScanStatusResponse =
	| {
			new_sast_issues_found?: number;
			new_iac_issues_found?: number;
			new_dependency_issues_found?: number;
			all_scans_completed: true;
			new_issues_found?: number;
			issue_links?: string[];
			diff_url?: string;
			gate_passed?: boolean;
	  }
	| {
			all_scans_completed: false;
	  };

export const startScan = async (secret: string, payload: Object): Promise<number> => {
	const url = `${AIKIDO_API_URL}/api/integrations/continuous_integration/scan/repository`;

	let response;

	try {
		response = await axios.post(url, payload, {
			headers: {
				'X-AIK-API-SECRET': secret,
				'Content-Type': 'application/json',
			},
		});
	} catch (error) {
		if (error instanceof AxiosError) {
			if (error.response?.status === 401) {
				throw new Error(`scan failed: unable to start scan: the provided secret_key is invalid`);
			}

			throw new Error(
				`scan failed: unable to start scan: request status "${
					error.response?.status
				}" returned with message: ${JSON.stringify(error.response?.data)}`
			);
		}

		throw new Error('Unable to start scan');
	}

	const { scan_id: scanId }: { scan_id: number } = response.data;

	if (!scanId) {
		throw new Error(
			`scan failed: unable to start scan: no scan_id received in the response: ${JSON.stringify(response.data)}`
		);
	}

	return scanId;
};

export const getScanStatus = (secret: string, scanId: number): (() => Promise<TGetScanStatusResponse>) => {
	return async (): Promise<TGetScanStatusResponse> => {
		const url = new URL(`${AIKIDO_API_URL}/api/integrations/continuous_integration/scan/repository`);
		url.searchParams.set('scan_id', scanId.toString());

		try {
			const response = await axios.get(url.toString(), {
				headers: {
					'X-AIK-API-SECRET': secret,
					'Content-Type': 'application/json',
				},
			});

			return response.data;
		} catch (error) {
			if (error instanceof AxiosError) {
				throw new Error(
					`scan failed: failed to check scan progress: did not receive a good result: ${
						error.response?.status
					}, message: ${JSON.stringify(error.response?.data)}`
				);
			}

			throw new Error("scan failed: an error occured while checking the scan's status");
		}
	};
};
