export class CriticalIssuesDetectedException extends Error {
	constructor(issueCount: number) {
		super(`dependency scan completed: found ${issueCount} new critical issue(s).`);
	}
}
