export class TimeoutReachedException extends Error {
	constructor(timeoutInSeconds: number) {
		super(`scan failed: the scan did not return any results within ${timeoutInSeconds} seconds`);
	}
}
