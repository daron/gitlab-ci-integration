import type { TInputContext, TInputProperties } from '../types';
import { getScanStatus, startScan } from './api';
import { TimeoutReachedException } from './exceptions/timeoutReached.exception';
import { getCurrentUnixTime, sleep } from './time';

export const scanCode = async (input: TInputProperties, context: TInputContext): Promise<void> => {
	const startScanPayload = {
		version: '1.0.5',
		branch_name: context.ref,
		repository_id: context.projectId,
		base_commit_id: context.startCommitId,
		head_commit_id: context.endCommitId,
		author: context.author,
		pull_request_metadata: {
			title: context.pullRequestMeta.title,
			url: context.pullRequestMeta.url,
		},

		// user config
		fail_on_dependency_scan: input.failOnDependencyScan,
		fail_on_sast_scan: input.failOnSastScan,
		fail_on_iac_scan: input.failOnIacScan,
		minimum_severity: input.minimumSeverity,
	};

	const scanId = await startScan(input.secretKey, startScanPayload);

	console.log(`successfully started a scan with id: "${scanId}"`);

	const getScanCompletionStatus = getScanStatus(input.secretKey, scanId);

	const expirationTimestamp = getCurrentUnixTime() + input.timeoutSeconds * 1000; // 2 minutes from now

	let scanIsCompleted = false;

	console.log('==== check if scan is completed ====');

	do {
		const result = await getScanCompletionStatus();

		if (!result.all_scans_completed) {
			console.log('==== scan is not yet completed, wait a few seconds ====');
			await sleep(5000);

			const dependencyScanTimeoutReached = getCurrentUnixTime() > expirationTimestamp;
			if (dependencyScanTimeoutReached) {
				// no need to check on fail_on_timeout prop since its checked above
				throw new TimeoutReachedException(input.timeoutSeconds);
			}

			continue;
		}

		scanIsCompleted = true;

		const {
			gate_passed = false,
			new_issues_found = 0,
			issue_links = [],
			new_dependency_issues_found = 0,
			new_iac_issues_found = 0,
			new_sast_issues_found = 0,
			diff_url,
		} = result;

		let moreDetailsText = '';
		if (diff_url) {
			moreDetailsText = ` More details at ${diff_url}`;
		}

		if (!gate_passed) {
			for (const linkToIssue of issue_links) {
				throw new Error(
					`scan completed with error: new issue detected with severity >=${input.minimumSeverity}. Check it out at: ${linkToIssue}`
				);
			}

			throw new Error(
				`scan completed with error: found ${new_issues_found} new issues with severity >=${input.minimumSeverity}.${moreDetailsText}`
			);
		}

		if (new_dependency_issues_found > 0 && input.failOnDependencyScan) {
			throw new Error(
				`scan completed with error: ${new_dependency_issues_found} new dependency issue(s) detected.${moreDetailsText}`
			);
		}
		if (new_iac_issues_found > 0 && input.failOnIacScan) {
			throw new Error(
				`scan completed with error: ${new_iac_issues_found} new IaC issue(s) detected.${moreDetailsText}`
			);
		}
		if (new_sast_issues_found > 0 && input.failOnDependencyScan) {
			throw new Error(
				`scan completed with error: ${new_sast_issues_found} new SAST issue(s) detected.${moreDetailsText}`
			);
		}
	} while (!scanIsCompleted);

	console.log(`scan completed: no new critical issues were found`);
};
