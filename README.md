# Aikido Security Gitlab CI security scanner

This repository contains an application that can be used in Github action workflows. It will trigger a scan in Aikido to make sure no new critical issues are introduced into your application. This action is available in any paid plan at Aikido.

## Using this job

We recommend to only run this job on merge requests. This will make sure only your latest changes are scanned and the scan will happen faster. It is therefore important to include the following bit to the job:

```yaml
only:
    - merge_requests
```

Firstly, you need to obtain the CI Integration Token from the Aikido settings page and add it to the variables of your CI CD pipeline in Gitlab. You can do this by going to Aikido's integration page and click on "Connect" for Gitlab CI. Next you can click on "Start integrating" to obtain a secret which can be used in the next step.

You can add this variable to the pipeline's environment by going to Settings > CI/CD and then click on "expand" next to "Variables". Make sure the variable is available on all branches (uncheck protect variable) and is masked in any logs (check the box to "Mask variable") you can leave "Expand variable reference" checked. As variable you can use, and we use in the samples below, "AIKIDO_SECRET_KEY", the value must be the secret that you copied from Aikido's CI integration settings page.

Next you need to create a `.gitlab-ci.yml` file at the root of your project to define the CI's jobs. Then you can use our dockerized CLI to run the scan as a job using the sample below:

```yaml
stages:
    - verify

aikido_security_dependency_scanning:
    stage: verify
    image:
        name: registry.gitlab.com/aikido-security/gitlab-ci-integration/dependency-scanner:latest
        entrypoint: ['/bin/sh', '-c']
    only:
        - merge_requests
    script:
        - node /app/index.js --secret_key $AIKIDO_SECRET_KEY
```

By default, the job will fail if the scan did not complete within 2 minutes. You can control this behaviour by providing the `fail_on_timeout` parameter to the script and set it to false:

```yaml
stages:
    - verify

aikido_security_dependency_scanning:
    stage: verify
    image:
        name: registry.gitlab.com/aikido-security/gitlab-ci-integration/dependency-scanner:latest
        entrypoint: ['/bin/sh', '-c']
    only:
        - merge_requests
    script:
        - node /app/index.js --secret_key $AIKIDO_SECRET_KEY --fail_on_timeout false
```

This integration will block a pipeline when it detects of critical severity by default. If you'd like to be more conservative and fail on another severity, you can also do so via a parameter to the script:

```yaml
stages:
    - verify

aikido_security_dependency_scanning:
    stage: verify
    image:
        name: registry.gitlab.com/aikido-security/gitlab-ci-integration/dependency-scanner:latest
        entrypoint: ['/bin/sh', '-c']
    only:
        - merge_requests
    script:
        - node /app/index.js --secret_key $AIKIDO_SECRET_KEY --minimum_severity MEDIUM
```

Now the action will still shut down after 2 minutes, but it won't fail and block your pipeline.

Below we listed a table with all properties you can provide to the action

| property                | required | default  | description                                                                                                                             |
| ----------------------- | -------- | -------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| secret_key              | yes      | N/A      | The secret you generated when enabling the CI integration in your Aikido account                                                        |
| fail_on_timeout         | no       | yes      | Whether or not the scan will fail in case of a timeout                                                                                  |
| timeout_seconds         | no       | 120      | The amount of seconds the scan runs before throwing a timeout error                                                                     |
| fail_on_dependency_scan | no       | true     | Whether or not the scan will fail if new open source dependency issues are detected                                                     |
| fail_on_sast_scan       | no       | false    | Whether or not the scan will fail if new SAST issues are detected                                                                       |
| fail_on_iac_scan        | no       | false    | Whether or not the scan will fail if new IAC issues are detected                                                                        |
| minimum_severity        | no       | CRITICAL | The minimum severity of the issues which will cause the scan to fail. Must be one of the following values: CRITICAL, HIGH, MEDIUM, LOW. |

## Development

To start off, run `npm install`.

Next, you can start the script by running `npm run dev` and provide any parameters like such `npm run dev -- --secret_key` the `--` is needed to pass the arguments to the npm script.
